import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../Header';
import Form from '../Form';
import Joke from '../Joke';
import {getJoke} from '../../store/actions';
import './App.css';

class App extends Component {

  getRandomJoke = async() => {
    const {dispatch} = this.props;
    dispatch(getJoke());
    console.log(this.state);
  }

  render() {
    return (
      <div>
        <Header/>
        <Form clicked={this.getRandomJoke}/>
        <Joke/>
      </div>
    );
  };
}

const mapStateToProps = state => ({
  joke: state.joke
});

export default connect(mapStateToProps)(App);