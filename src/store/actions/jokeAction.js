import {CONSTANTS} from './index';

export const getJoke = (data) => {
  return {
    type: CONSTANTS.GET_JOKE_ASYNC,
    data: data
  };
};