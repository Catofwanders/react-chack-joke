import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import {whatchJokes} from './sagas';
import rootReducer from './reducers/index';


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const SagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer,composeEnhancers(
  applyMiddleware(thunk, SagaMiddleware)
));

SagaMiddleware.run(whatchJokes);


export default store;