import {takeEvery} from 'redux-saga/effects';
import {getJokeSaga} from './jokeSaga';
import {CONSTANTS} from '../actions';

export function* whatchJokes() {
  yield takeEvery(CONSTANTS.GET_JOKE_ASYNC, getJokeSaga)
}