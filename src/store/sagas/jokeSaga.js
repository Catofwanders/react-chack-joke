import {put} from 'redux-saga/effects';
import axios from 'axios';
import * as actions from '../actions';

export function* getJokeSaga(action){
  try {
    const response = yield axios.get('https://api.chucknorris.io/jokes/random');
    console.log(response.data.value);
    yield put(actions.getJoke(response.data));
  } catch(err) {
    // yield put(actions.fetchIngredientsFailed());
  }
};